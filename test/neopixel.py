# simple neopixel tests

import board
import neopixel

print("define pixels")
pixels = neopixel.NeoPixel(board.D0, 16)

while True:
	print("Setting neopixel to red")
	pixels.fill(255, 0, 0)

# eof
