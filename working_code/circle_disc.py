# simple circle for disc

import time
import board
import neopixel

print("define pixel disc")
DISC_PIN = board.D0
DISC_PIXEL_COUNT = 16
DISC_BRIGHTNESS = 0.1
DISC_AUTO_WRITE = False # changes are not applied automatically, but after xyz.show()
pixel_disc = neopixel.NeoPixel(DISC_PIN, DISC_PIXEL_COUNT, brightness=DISC_BRIGHTNESS, auto_write=DISC_AUTO_WRITE)

def color_chase(color, wait):
	for i in range(DISC_PIXEL_COUNT):
		pixel_disc[i] = color
		time.sleep(wait)
		pixel_disc.show()

while True:

	color_chase((0, 0, 255), .2)
	color_chase((0, 255, 0), .2)
	color_chase((255, 0, 0), .2)

# eof
