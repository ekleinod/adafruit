# simple led tests

import time
import digitalio
import board
import adafruit_dotstar

print("define led")
led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

print("define multicolor led")
multicolorled = adafruit_dotstar.DotStar(board.APA102_SCK, board.APA102_MOSI, 1)
multicolorled.brightness = 0.1

while True:

	led.value = True
	multicolorled[0] = (0, 0, 255)
	time.sleep(0.5)

	led.value = False
	multicolorled[0] = (255, 0, 255)
	time.sleep(0.5)


# eof
