# simple rainbow for disc

import time
import board
import neopixel
import rainbowio

print("define pixel disc")
DISC_PIN = board.D0
DISC_PIXELS = 16
DISC_BRIGHTNESS = 0.1
DISC_AUTO_WRITE = False # changes are not applied automatically, but after xyz.show()
pixels = neopixel.NeoPixel(DISC_PIN, DISC_PIXELS, brightness=DISC_BRIGHTNESS, auto_write=False)

i = 0
while True:

	i = (i + 1) % 256

	pixels.fill(rainbowio.colorwheel(i))
	pixels.show()

	time.sleep(0.01)

# eof
