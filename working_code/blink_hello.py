# let the red LED blink and print Hello
# https://learn.adafruit.com/adafruit-trinket-m0-circuitpython-arduino/creating-and-editing-code

import board
import digitalio
import time

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

while True:
	led.value = True
	time.sleep(0.5)
	led.value = False
	time.sleep(0.5)
	print("Hello!")
