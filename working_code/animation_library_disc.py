# animation library for disc

import time
import board
import neopixel
import rainbowio

import adafruit_led_animation.color
from adafruit_led_animation.animation.chase import Chase
# from adafruit_led_animation.animation.comet import Comet
# from adafruit_led_animation.animation.solid import Solid
# from adafruit_led_animation.animation.colorcycle import ColorCycle
# from adafruit_led_animation.animation.blink import Blink

print("define pixel disc")
DISC_PIN = board.D0
DISC_PIXEL_COUNT = 16
DISC_BRIGHTNESS = 0.1
DISC_AUTO_WRITE = False # changes are not applied automatically, but after xyz.show()
pixel_disc = neopixel.NeoPixel(DISC_PIN, DISC_PIXEL_COUNT, brightness=DISC_BRIGHTNESS, auto_write=DISC_AUTO_WRITE)

print("define animation")
animation = Chase(pixel_disc, speed=0.1, color=adafruit_led_animation.color.AMBER, size=2, spacing=2)
# animation = Comet(pixel_disc, speed=0.05, color=adafruit_led_animation.color.PURPLE, tail_length=8, bounce=False)
# animation = Solid(pixel_disc, color=adafruit_led_animation.color.PINK)
# animation = ColorCycle(pixel_disc, speed=0.2, colors=[adafruit_led_animation.color.MAGENTA, adafruit_led_animation.color.ORANGE, adafruit_led_animation.color.TEAL])
# animation = Blink(pixel_disc, speed=1, color=adafruit_led_animation.color.AMBER)

while True:

	animation.animate()

# eof
