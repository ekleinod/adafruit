# rainbow circle for disc

import time
import board
import neopixel
import rainbowio

print("define pixel disc")
DISC_PIN = board.D0
DISC_PIXEL_COUNT = 16
DISC_BRIGHTNESS = 0.1
DISC_AUTO_WRITE = False # changes are not applied automatically, but after xyz.show()
pixel_disc = neopixel.NeoPixel(DISC_PIN, DISC_PIXEL_COUNT, brightness=DISC_BRIGHTNESS, auto_write=DISC_AUTO_WRITE)

color_index = 0
while True:

	for pixel_index in range(DISC_PIXEL_COUNT):
		color_index = (color_index + 5) % 256
		pixel_disc[pixel_index] = rainbowio.colorwheel(color_index)
		pixel_disc.show()
		time.sleep(0.1)

# eof
