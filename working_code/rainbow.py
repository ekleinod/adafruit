# simple rainbow

import time
import board
import adafruit_dotstar
import rainbowio

print("define multicolor led")
multicolorled = adafruit_dotstar.DotStar(board.APA102_SCK, board.APA102_MOSI, 1)
multicolorled.brightness = 0.1

i = 0
while True:

	i = (i + 1) % 256
	multicolorled.fill(rainbowio.colorwheel(i))
	time.sleep(0.01)

# eof
